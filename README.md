# Combinação da estimativa por máxima verossimilhança com uma rede neural profunda em uma abordagem para predizer distribuições de confiabilidade para ativos com histórico desconhecido

Repositório de códigos e bases de dados de exemplo para avaliação e aplicabilidade, referente ao seguinte artigo:
G. Scavariello, A. Picanço and Cristiano Torezzan, “Estimativa de máxima verossimilhança para curvas de confiabilidade por meio de redes neurais profundas: um estudo com base em dados reais do setor sucroenergético,” IEEE Lat. Am. Trans., 2021.
