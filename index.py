# -*- coding: utf-8 -*-
"""
This is the main file of a python script intended to implement the procedures of the project below:
'Estimating the best distribution to predict reliability and failure for industrial assets with no failures history'
Created on Sun Jun 28 12:36:40 2020

@authors: Guilherme Hering Scavariello - guilherme.scavariello@gmail.com
Cristiano Torezzan
Ailson Picanço
@university: UNICAMP - Campinas - SP, Brazil
@program: Master of Science in Producting and Manufacturing Engineering. Emphasis on Operations Research. 
@advisor: Cristiano Torezzan
"""

import pandas as pd
import numpy as np
from datetime import datetime
from reliability.Fitters import Fit_Everything

from sklearn.preprocessing import OneHotEncoder

import tensorflow as tf

from tensorflow import feature_column
from tensorflow.keras import layers
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from keras.utils import np_utils
from keras.layers import Dense, LSTM
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import LabelEncoder
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers import Dense, LSTM
from numpy import array
from numpy.random import uniform
from numpy import hstack
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from sklearn.datasets import make_regression
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import RepeatedKFold


assets = pd.read_csv('assetslist.csv',sep=';',decimal=",")  
orders = pd.read_csv('orders.csv',sep=';',decimal=",")  


#Converting date formats
orders['Date'] = pd.to_datetime(orders['Date'], format='%d/%m/%Y')

#filtering only maintenances assumed as considerable corrective intervention.
#Selecting only maintenances orders that have stopped the machine and requested an immediate intervention,
#or that have demanded a planned stop that did impact the production.

#All the emergency maintenance orders were considered,
#but only planned maintenance orders with values higher than the value
# in BRL (R$) defined below are considered on the filter.
#This selection is made to avoid considering planned maintenance orders with low significance. 
filteredcosts = 500
orders = orders[((orders['Type of order']=='OM02') & (orders['Total Costs']>=filteredcosts)) | (orders['Type of order']=='OM01')]


#create a new pivot dataframe with three new columns, one that will be filled with
#the number of failures, one that will be filled with the data of the failures, and
# another will hold the failures intervals


pivot = pd.pivot_table(orders, index=['Hierarchy Structure'], values=['Type of order'], aggfunc=len,fill_value=0)
pivot = pivot.reset_index()
pivot.rename(columns={'Type of order':'Number of failures'}, inplace=True)

#orders[orders["Hierarchy Structure"=="04-I-GE-GE1-GE-GE01-ACPR-TUR01"]]

pivot['Failures Dates'] = None
pivot['Failures Array'] = None

for index, row in pivot.iterrows():
    failuredates = pd.DataFrame
    failuredates = orders[orders['Hierarchy Structure'] == row['Hierarchy Structure']]['Date']
    
    #declares "day zero" as the day that the measure of maintenance orders begins
    #in this case, its 1st April 2019
    day0 = "01-04-2019"
    day0 = datetime.strptime(day0, "%d-%m-%Y")
    
    failuredates = failuredates.reset_index()
    failuredates = failuredates.sort_values(by='Date').reset_index()
    

    failuredays = []
    for i, row in failuredates.iterrows():
        if i==0:
            
            failuredays.append((failuredates.iloc[i]['Date'] - day0).days)
        else:
            failuredays.append((failuredates.iloc[i]['Date'] - failuredates.iloc[i-1]['Date']).days)
    
    pivot.at[index,'Failures Dates'] = np.array(failuredates['Date'])
    pivot.at[index,'Failures Array'] = np.array(failuredays)


#merging the assets dataframe with the pivot dataframe, to have one main
#dataframe containing all the assets and its failures informations

assets['Failures Array'] = None
assets['Failures Dates'] = None
assets['Number of failures'] = None
for index, row in assets.iterrows():
    if sum(pivot['Hierarchy Structure']==row['Hierarchy Structure'])>0:
        fnumber = pivot[pivot['Hierarchy Structure']==row['Hierarchy Structure']]['Number of failures']
        assets.at[index,'Number of failures'] = fnumber.iloc[0]
        fdates = pivot[pivot['Hierarchy Structure']==row['Hierarchy Structure']]['Failures Dates']
        assets.at[index,'Failures Dates'] = np.array(fdates.iloc[0])
        fdays = pivot[pivot['Hierarchy Structure']==row['Hierarchy Structure']]['Failures Array']
        assets.at[index,'Failures Array'] = np.array(fdays.iloc[0])

#Setting groups of equipment by maker and model, without taking into account
#other attributes such as the site location and the age of
#each equipment. It had to be done due to the poor number of failures record.
#This step can be ignored if your dataset holds a greater
#number of records.

groups = pd.pivot_table(assets, index=['Model','Maker'], values=['Hierarchy Structure'], aggfunc=len)
groups = groups.reset_index()

#Joining the failures data of every asset that belongs to each group.

groups['Failures Array'] = None
groups['Failures Dates'] = None
groups['Number of failures'] = None
for index, row in groups.iterrows():
    groups.at[index,'Number of failures'] = assets[(assets['Model']==row['Model']) & (assets['Maker']==row['Maker'])]['Number of failures'].sum()
    
    fdates = assets[(assets['Maker']==row['Maker']) & (assets['Model']==row['Model']) ]['Failures Dates']
    fdates = fdates.to_frame()
    fdates2 = np.array(None)
    for index2, row2 in fdates.iterrows():
        if np.all(row2['Failures Dates'] != None):
            if np.all(fdates2 == None):
                fdates2 = row2['Failures Dates']
            else:
                fdates2 = np.concatenate([fdates2,row2['Failures Dates']])
    groups.at[index,'Failures Dates'] = fdates2
    
    fdays = assets[(assets['Maker']==row['Maker']) & (assets['Model']==row['Model']) ]['Failures Array']
    fdays = fdays.to_frame()
    fdays2 = np.array(None)
    for index2, row2 in fdays.iterrows():
        if np.all(row2['Failures Array'] != None):
            if np.all(fdays2 == None):
                fdays2 = row2['Failures Array']
            else:
                fdays2 = np.concatenate([fdays2,row2['Failures Array']])
    groups.at[index,'Failures Array'] = fdays2
       
#with less than 3 failure records, it is not advisable to use the MLE to find the
#parameters for most of the supported distributions (like 3 degrees of freedom)
    
groups2 = groups[groups['Number of failures']>=3]
groups2 = groups2.reset_index()

groups2 = groups2.drop(['index'],axis=1)

#given the sample provided, we ended up figuring out that we had only 9 groups of
#assets that could have their distribution found by MLE

#applying MLE using python librar reliability function fit_evertything.
#we compare the AICcs and BICs find the best distribution and its parameters.
#AICc (Akaike Information Criterion corrected) and BIC (Bayesian Information Criterion)


groups2['Distribution'] = None
groups2['Alpha'] = None
groups2['Beta'] = None
groups2['Gamma'] = None
groups2['Mu'] = None
groups2['Sigma'] = None
groups2['Lambda'] = None
for index, row in groups2.iterrows():

#filtering  failures that happens in the same day (probably the same failure)
    farrayfull = row['Failures Array'] 
    
    results = Fit_Everything(failures=farrayfull[farrayfull!=0],show_histogram_plot=False, show_probability_plot=False, show_PP_plot=False, print_results=False)
    groups2.at[index,'Distribution'] = results.best_distribution_name
    if results.best_distribution.parameters.shape[0] >= 1: groups2.at[index,'Alpha'] = results.best_distribution.parameters[0]
    if results.best_distribution.parameters.shape[0] >= 2: groups2.at[index,'Beta'] = results.best_distribution.parameters[1]
    if results.best_distribution.parameters.shape[0] >= 3: groups2.at[index,'Gamma'] =  results.best_distribution.parameters[2]
    if results.best_distribution.parameters.shape[0] >= 4 :groups2.at[index,'Mu'] = results.best_distribution.parameters[3]
    if results.best_distribution.parameters.shape[0] >= 5 :groups2.at[index,'Sigma'] = results.best_distribution.parameters[4]
    if results.best_distribution.parameters.shape[0] >= 6 :groups2.at[index,'Lambda'] = results.best_distribution.parameters[5]


#converting MLE parameters to floaat
groups2['Alpha'] = groups2['Alpha'].astype(float) 
groups2['Beta'] = groups2['Beta'].astype(float)    
groups2['Gamma'] = groups2['Gamma'].astype(float)    
groups2['Mu'] = groups2['Mu'].astype(float)    
groups2['Sigma'] = groups2['Sigma'].astype(float)    
groups2['Lambda'] = groups2['Lambda'].astype(float)    

#As we observe, the dataframe "groups2" has only 9 'useful' samples. This might
#not be enought to train a ML model. So that, lets run the MLE under the same assumptions, but now
#applied on the original asset data, before the grouping steps


#with less than 3 failure records, it is not advisable to use the MLE to find the
#parameters for most of the supported distributions (like 3 degrees of freedom)

assets = assets.replace({'Number of failures':{None:0}})

assets2 = assets[assets['Number of failures']>=3]
assets2 = assets2.reset_index()

assets2 = assets2.drop(['index'],axis=1)


assets2['Distribution'] = None
assets2['Alpha'] = None
assets2['Beta'] = None
assets2['Gamma'] = None
assets2['Mu'] = None
assets2['Sigma'] = None
assets2['Lambda'] = None
for index, row in assets2.iterrows():

#filtering  failures that happens in the same day (probably the same failure)
    farrayfull = row['Failures Array'] 
    
    results = Fit_Everything(failures=farrayfull[farrayfull!=0],show_histogram_plot=False, show_probability_plot=False, show_PP_plot=False, print_results=False)
    assets2.at[index,'Distribution'] = results.best_distribution_name
    if results.best_distribution.parameters.shape[0] >= 1: assets2.at[index,'Alpha'] = results.best_distribution.parameters[0]
    if results.best_distribution.parameters.shape[0] >= 2: assets2.at[index,'Beta'] = results.best_distribution.parameters[1]
    if results.best_distribution.parameters.shape[0] >= 3: assets2.at[index,'Gamma'] =  results.best_distribution.parameters[2]
    if results.best_distribution.parameters.shape[0] >= 4 :assets2.at[index,'Mu'] = results.best_distribution.parameters[3]
    if results.best_distribution.parameters.shape[0] >= 5 :assets2.at[index,'Sigma'] = results.best_distribution.parameters[4]
    if results.best_distribution.parameters.shape[0] >= 6 :assets2.at[index,'Lambda'] = results.best_distribution.parameters[5]
    
    
#converting MLE parameters to floaat
assets2['Alpha'] = assets2['Alpha'].astype(float) 
assets2['Beta'] = assets2['Beta'].astype(float)    
assets2['Gamma'] = assets2['Gamma'].astype(float)    
assets2['Mu'] = assets2['Mu'].astype(float)    
assets2['Sigma'] = assets2['Sigma'].astype(float)    
assets2['Lambda'] = assets2['Lambda'].astype(float)       




#Splitting the dataframe into input (X) and output (y) for modeling a machine learning.

X = assets2.iloc[:,:7]
X = X.drop(columns='Hierarchy Structure')

y = assets2.iloc[:,10:]

y['Distribution'] = y['Distribution'].astype(str)

y = y.replace({np.nan:0})



# The input and output data contains different string values.
#When modeling multi-class classification problems using neural networks,
#it is good practice to reshape the output attribute from a vector that contains
#values for each class value to be a matrix with a boolean for each class value
# and whether or not a given instance has that class value or not.
# In this case, the data will be encoded with One Hot Encode

#A one hot encoding is appropriate for categorical data where no relationship exists
# between categories.

X_categories = X.drop(columns=['Age'])

X_number = X['Age']

def prepare_inputs(X):
	ohe = OneHotEncoder(sparse=False)
	ohe.fit(X)
	X_enc = ohe.transform(X)
	return X_enc

X_enc1 = prepare_inputs(X_categories)

#joining 'binarized' categories  and numeric fields,
#to finally get the data prepared for ML
Xprepared = np.hstack((X_enc1,np.array(X_number).reshape(-1,1)))

yprepared = np.hstack((prepare_inputs(np.array(y.iloc[:,0]).reshape(-1,1)),y.iloc[:,1:]))

#Defining and fitting the model
#We'll start by defining the sequential model.
#The sequential model contains LSTM layers with
#ReLU activations, Dense output layer,  and Adam optimizer
# with MSE loss function. We'll set the input dimension in
#the first layer and output dimension in the last layer of the model.

#The LSTM model input dimension requires the third dimension
# that will be the number of the single input row.
#We'll reshape the x data.
Xprepared2 = Xprepared.reshape(Xprepared.shape[0], Xprepared.shape[1], 1)

#Next, we'll define the input and output data dimensions from x and data.
in_dim = (Xprepared2.shape[1], Xprepared2.shape[2])
out_dim = yprepared.shape[1]
 

# split data into train and test sets
#X_train, X_test, y_train, y_test = train_test_split(Xprepared2, yprepared, test_size=0.1, random_state=1)

#due to the very low number of samples, it was decided 
#to RE-USE the training data as test
X_train = Xprepared2
X_test = X_train

y_train = yprepared
y_test = y_train

model = Sequential()
#Defining a model with LSTM units in the hidden layer
hidden_size = 128


#model.add(LSTM(hidden_size,return_sequences=True, input_shape=in_dim, activation="relu"))
model.add(LSTM(hidden_size, input_shape=in_dim, activation="relu"))
# and an output layer that predicts multiple outputs
model.add(Dense(out_dim))
model.compile(loss="mse", optimizer="adam") 
 
model.summary()

#fitting the model with train data.
model.fit(X_train, y_train, epochs=100, batch_size=12, verbose=0)

# predicting test data and check the mean squared error rate.
ypred = model.predict(X_test)


print("y0 MSE:%.4f" % mean_squared_error(y_test[:,0], ypred[:,0]))
print("y1 MSE:%.4f" % mean_squared_error(y_test[:,1], ypred[:,1])) 
print("y2 MSE:%.4f" % mean_squared_error(y_test[:,2], ypred[:,2])) 
print("y3 MSE:%.4f" % mean_squared_error(y_test[:,3], ypred[:,3])) 


#To measure the model's accuracy, we use the mean squared error (MSE)
#it measures the average of the squares of the errors—that is, 
#the average squared difference between the estimated values and
#the actual value.

x_ax = range(len(X_test))
plt.title("LSTM multi-output prediction")
plt.scatter(x_ax, y_test[:,0],  s=6, label="y0-test")
plt.plot(x_ax, ypred[:,0], label="y0-pred")
plt.scatter(x_ax, y_test[:,1],  s=6, label="y1-test")
plt.plot(x_ax, ypred[:,1], label="y1-pred")
plt.scatter(x_ax, y_test[:,2],  s=6, label="y2-test")
plt.plot(x_ax, ypred[:,2], label="y2-pred")
plt.scatter(x_ax, y_test[:,3],  s=6, label="y3-test")
plt.plot(x_ax, ypred[:,3], label="y3-pred")
plt.legend()
plt.show()

# evaluate multioutput regression model 
model.evaluate(X_train, y_train)
print(model.metrics_names)




# is it possible to apply ROC curve into this model?
